/* Done by Deon
    Make sure you have these permissions set in your manifest.
    <uses-permission android:name="android.permission.READ_CALENDAR" />
    <uses-permission android:name="android.permission.WRITE_CALENDAR" />
*/

package <YOUR_PACKAGE_NAME_HERE>;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract.Calendars;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Reminders;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static com.google.common.reflect.Reflection.initialize;

public class CalendarPage extends Activity {
    private static final int PERMISSIONS_GRANTED = 1;
    private static final String[] PERMISSIONS_REQUIRED = new String[] {
            Manifest.permission.READ_CALENDAR,
            Manifest.permission.WRITE_CALENDAR
    };

    private TextView textView;
    private Cursor cursor;

    // find more columns to get at: https://developer.android.com/reference/android/provider/CalendarContract.Calendars
    public static final String[] CALENDER_COLUMNS_TO_GET = new String[]{
            Calendars._ID,                      // column 0
            Calendars.ACCOUNT_NAME,             // column 1
            Calendars.CALENDAR_DISPLAY_NAME,    // column 2
            Calendars.ACCOUNT_TYPE,             // column 3
            Calendars.CALENDAR_ACCESS_LEVEL,    // column 4
            Calendars.IS_PRIMARY                // column 5
    };

    // find more columns to get at: https://developer.android.com/reference/android/provider/CalendarContract.Events
    public static final String[] EVENT_COLUMNS_TO_GET = new String[]{
            "_id",                              // column 0
            Events.TITLE,                       // column 1
            Events.EVENT_LOCATION,              // column 2
            Events.DTSTART,                     // column 3
            Events.DTEND                        // column 4
    };

    public static final String[] REMINDER_COLUMNS_TO_GET = new String[]{
            Reminders.EVENT_ID,                 // column 0
            Reminders.MINUTES,                  // column 1
            Reminders.METHOD                    // column 2
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_page);
        checkPermissions();
        textView = findViewById(R.id.textView);
        Button button_query_calendars = findViewById(R.id.button_query_calendars);
        Button button_query_events = findViewById(R.id.button_query_events);
        Button button_query_reminders = findViewById(R.id.button_query_reminders);
        Button button_add_event = findViewById(R.id.button_add_event);
        button_query_calendars.setOnClickListener(onClickListener_button_query_calendars);
        button_query_events.setOnClickListener(onClickListener_button_query_events);
        button_query_reminders.setOnClickListener(onClickListener_button_query_reminders);
        button_add_event.setOnClickListener(onClickListener_button_add_event);

    }

    public View.OnClickListener onClickListener_button_query_calendars = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            queryCalendars();
        }
    };

    public View.OnClickListener onClickListener_button_query_events = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            queryEvents();
        }
    };

    public View.OnClickListener onClickListener_button_query_reminders = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            queryReminders();
        }
    };

    public View.OnClickListener onClickListener_button_add_event = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            addEvent();
        }
    };

    public void queryCalendars(){
        textView.setText("");
        cursor = getContentResolver().query(Calendars.CONTENT_URI, CALENDER_COLUMNS_TO_GET, null, null, null, null);
        while (cursor.moveToNext()) {
            if (cursor != null) {
                String _0 = cursor.getColumnName(0) + ": " + cursor.getString(0);
                String _1 = cursor.getColumnName(1) + ": " + cursor.getString(1);
                String _2 = cursor.getColumnName(2) + ": " + cursor.getString(2);
                String _3 = cursor.getColumnName(3) + ": " + cursor.getString(3);
                String _4 = cursor.getColumnName(4) + ": " + cursor.getString(4);
                String _5 = cursor.getColumnName(5) + ": " + cursor.getString(5);
                Log.d("calendars", "." + "\n" +
                        _0 + "\n" +
                        _1 + "\n" +
                        _2 + "\n" +
                        _3 + "\n" +
                        _4 + "\n" +
                        _5 + "\n.");
                textView.append("." + "\n" +
                        _0 + "\n" +
                        _1 + "\n" +
                        _2 + "\n" +
                        _3 + "\n" +
                        _4 + "\n" +
                        _5 + "\n.");
            } else {
                Log.d("calendars", "no calendar present!");
                textView.append("no calendar present!");
            }
        }
    }

    public void queryEvents() {
        textView.setText("");
//        String selection = Events.ORGANIZER;        // Events.CALENDAR_ID; to search by calendar.
//        String[] selectionArgs = new String[]{      // if searching by calendar, set parameters to the corresponding calendar id.
//                "Singapore Polytechnic",            // this is searching for 2 organisers.
//                "Example Organiser"
//        };
        cursor = getContentResolver().query(Events.CONTENT_URI, EVENT_COLUMNS_TO_GET, null, null, null, null);
        while (cursor.moveToNext()) {
            if(cursor!=null){
                String _0 = cursor.getColumnName(0) + ": " + cursor.getString(0);
                String _1 = cursor.getColumnName(1) + ": " + cursor.getString(1);
                String _2 = cursor.getColumnName(2) + ": " + cursor.getString(2);
                String _3 = cursor.getColumnName(3) + ": " + cursor.getString(3);
                String _4 = cursor.getColumnName(4) + ": " + cursor.getString(4);
                Log.d("events", "." + "\n" +
                        _0 + "\n" +
                        _1 + "\n" +
                        _2 + "\n" +
                        _3 + "\n" +
                        _4 + "\n.");
                textView.append("." + "\n" +
                        _0 + "\n" +
                        _1 + "\n" +
                        _2 + "\n" +
                        _3 + "\n" +
                        _4 + "\n.");
            } else {
                Log.d("events", "no events present!");
                textView.append("no events present!");
            }
        }
    }

    public void queryReminders(){
        textView.setText("");
//        String selection = Events.ORGANIZER;        // Events.CALENDAR_ID; to search by calendar.
//        String[] selectionArgs = new String[]{      // if searching by calendar, set parameters to the corresponding calendar id.
//                "Singapore Polytechnic",            // this is searching for 2 organisers.
//                "Example Organiser"
//        };
        cursor = getContentResolver().query(Reminders.CONTENT_URI, REMINDER_COLUMNS_TO_GET, null, null, null, null);
        while (cursor.moveToNext()) {
            if(cursor!=null){
                String _0 = cursor.getColumnName(0) + ": " + cursor.getString(0);
                String _1 = cursor.getColumnName(1) + ": " + cursor.getString(1);
                String _2 = cursor.getColumnName(2) + ": " + cursor.getString(2);
                Log.d("events", "." + "\n" +
                        _0 + "\n" +
                        _1 + "\n" +
                        _2 + "\n.");
                textView.append("." + "\n" +
                        _0 + "\n" +
                        _1 + "\n" +
                        _2 + "\n.");
            } else {
                Log.d("reminders", "no reminders present!");
                textView.append("no reminders present!");
            }
        }
    }

    public void addEvent() {
        textView.setText("");
        ContentValues contentValues = new ContentValues();

        Calendar beginTime = Calendar.getInstance();
        beginTime.getTimeInMillis();                                                // current time
        beginTime.add(Calendar.MINUTE, 5);                                  // add 5 minutes to start time.
        // beginTime.set(2019, 2, 1, 10, 30);                                       // explicit event start time.

        // Calendar duration = Calendar.getInstance();
        // duration.set(0,0,0, 1, 0,0);   // duration of 1 hour (Y/M/D/h/m/s)

        Calendar endTime = Calendar.getInstance();
        endTime.getTimeInMillis();                                                  // current time.
        endTime.add(Calendar.MINUTE, 5);                                    // add 5 minutes to end time.
        endTime.add(Calendar.HOUR,1);                                       // add 1 hour to end time.
        // endTime.set(2019, 2, 1, 11, 30);                                         // explicit event end time.

        // for a full lists of things you can put, visit: https://developer.android.com/reference/android/provider/CalendarContract.Events
        contentValues.put(Events.DTSTART, beginTime.getTimeInMillis());             // event start time
        contentValues.put(Events.DTEND, endTime.getTimeInMillis());                 // event end time. set if event is non-recurring. else, don't.
        // contentValues.put(Events.DURATION, duration.getTimeInMillis());             // event duration. set if event is recurring. else, don't.
        // contentValues.put(Events.RRULE, "FREQ=WEEKLY;BYDAY=MO,WE,FR;INTERVAL=1");   // event recurrence rule. set if event is recurring. else, don't. https://www.textmagic.com/free-tools/rrule-generator
        contentValues.put(Events.EVENT_TIMEZONE, "Etc/GMT+8");                      // event timezone. String[] availableTimeZones = TimeZone.getAvailableIDs();
        contentValues.put(Events.CALENDAR_ID, 1);                                   // which calendar to add this event to. Cursor cursor = getContentResolver().query(Calendars.CONTENT_URI, Calendars._ID, null, null, null, null);
        contentValues.put(Events.TITLE, "Example Title");                           // event title.
        contentValues.put(Events.EVENT_LOCATION, "Singapore Polytechnic");          // event location.
        contentValues.put(Events.DESCRIPTION, "Example Description");               // event description.
        contentValues.put(Events.ORGANIZER, "Singapore Polytechnic");               // event organiser.

        contentValues.put(Events.HAS_ALARM, true);                                  // event has a reminder.

        Uri uri = getContentResolver().insert(Events.CONTENT_URI, contentValues);   // insert the event to the corresponding calendar id.

        // adding a reminder when contentValues.put(Events.HAS_ALARM, true);
        long event_id = Long.parseLong(uri.getLastPathSegment());                    // get the event id that we just sent.
        ContentValues contentValues_reminders = new ContentValues();
        contentValues_reminders.put(Reminders.EVENT_ID, event_id);
        contentValues_reminders.put(Reminders.METHOD, Reminders.METHOD_ALERT);
        contentValues_reminders.put(Reminders.MINUTES, 4);                           // set alarm 5 minutes before the event start time.

        getContentResolver().insert(Reminders.CONTENT_URI, contentValues_reminders); // insert the reminder to the corresponding event id.

        textView.append("Event added to default calendar.\n" +
                "Start Time: 5 minutes from now.\n" +
                "End Time: 1 Hour 5 minutes from now.\n" +
                "Reminder: 1 minute from now.\n" +
                "Please check your local calendar app.");
    }

    public void checkPermissions() {
        List<String> missingPermissions = new ArrayList<String>();
        for (String permission : PERMISSIONS_REQUIRED) {
            int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            String[] permissions = missingPermissions.toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, PERMISSIONS_GRANTED);
        } else {
            int[] grantResults = new int[PERMISSIONS_REQUIRED.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(PERMISSIONS_GRANTED, PERMISSIONS_REQUIRED, grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_GRANTED:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "Required permission '" + permissions[index] + "' not granted, exiting", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                initialize();
                break;
        }
    }
}