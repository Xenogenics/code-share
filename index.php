<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
// define variables and set to empty values
$nameErr = $emailErr = $genderErr = $websiteErr = "";
$name = $email = $gender = $comment = $website = "";

// check if redirected using post method
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    // check if empty
    if(empty($_POST["name"]))
    {
        $nameErr = "Name is required";
    }
    else
    {
        $name = test_input($_POST["name"]);
        // check for illegal characters
        if(!preg_match("/^[a-zA-Z ]*$/",$name))
        {
            $nameErr = "Only letters and white space allowed";
        }
    }
    
    if(empty($_POST["email"]))
    {
        $emailErr = "Email is required";
    }
    else
    {
        $email = test_input($_POST["email"]);
        // check for invalid format
        if(!preg_match("/([\w\-+\@[\w\-]+\.[\w\-]+)/", $email))
        {
            $emailErr = "Invalid email format";
        }
    }
    
    if(empty($_POST["website"]))
    {
        $website = "";
    }
    else
    {
        $website = test_input($_POST["website"]);
        // check for invalid format
        if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $website))
        {
            $websiteErr = "Invalid URL";
        }
    }
    
    if(empty($_POST["comment"]))
    {
        $comment = "";
    }
    else
    {
        $comment = test_input($_POST["comment"]);
    }
    
    if(empty($_POST["gender"]))
    {
        $genderErr = "Gender is required";
    }
    else
    {
        $gender = test_input($_POST["gender"]);
    }    
}

// input validation
function test_input($data)
{
    $data = trim($data);    // remove whitespace
    $data = stripslashes($data);    // remove blackslashes
    $data = htmlspecialchars($data);    // replace special characters with HTML entities
    return $data;
}

// check if cookie is present
if(!isset($_COOKIE["user"]))
{
    setcookie("user", $name, time()+10);
    setcookie("expire", time()+10, time()+10);
}

session_start();
if(!isset($_SESSION["user"]))
{
    $_SESSION["user"] = "Guest";
    $_SESSION["views"] = 0;
}
else
{
    $_SESSION["user"] = $name;
    $_SESSION["views"] += 1;
}

?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Home</title>
    </head>
    <body>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" >
            <table style="border-style: solid; border-width: 1px; border-spacing: 5px; padding: 5px;">
                <tr>
                    <td>Session Timestamp:</td>
                    <td><?php echo date("d/m/Y - h:i:s A");?></td>
                </tr>
                <tr>
                    <td>Cookie:</td>
                    <td>
                        <?php 
                        if(isset($_COOKIE["user"]))
                        {
                            echo $_COOKIE["user"];
                            echo " - Expires in: ";
                            echo ($_COOKIE["expire"]) - time();
                            echo " seconds";
                        }
                        else
                        {
                            echo "Guest";
                            echo " - Expires in: never";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Session:</td>
                    <td>
                        <?php
                        if(isset($_SESSION["user"]) && $_SESSION["user"] != "")
                        {
                            echo $_SESSION["user"];
                            echo " clicked POST ";
                            echo $_SESSION["views"];
                            echo " times";
                        }
                        else
                        {
                            echo "Guest";
                            echo " clicked POST ";
                            echo $_SESSION["views"];
                            echo " times";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td><input type="text" name="name" value="<?php echo $name?>"><span class="error" style="color:red"> * <?php echo $nameErr; ?></span></td>
                </tr>
                <tr>
                    <td>E-mail: </td>
                    <td><input type="text" name="email" value="<?php echo $email?>"><span class="error" style="color:red"> * <?php echo $emailErr; ?></td>
                </tr>
                <tr>
                    <td>Website:</td>
                    <td><input type="text" name="website" value="<?php echo $website?>"><span class="error" style="color:red"><?php echo " * $websiteErr"; ?></td>
                </tr>
                <tr>
                    <td>Comment:</td>
                    <td><textarea name="comment" rows="5" cols="40"><?php echo $comment?></textarea></td>
                </tr>
                <tr>
                    <td>Gender:</td>
                    <td><input type="radio" name="gender" <?php if(isset($gender) && $gender == "female") {echo "checked";} ?> value="female">Female
                        <input type="radio" name="gender" <?php if(isset($gender) && $gender == "male") {echo "checked";} ?> value="male">Male
                        <span class="error" style="color:red"> * <?php echo $genderErr; ?></td>
                </tr>            
                <tr>
                    <td><input type="submit" value="POST"></td>
                    <td><span style="color:red"> * Required</span></td>
                </tr>
            </table>
        </form>
    </body>
</html>